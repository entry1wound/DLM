README for DLM testbed

Synopsis: 
    DLM is going to be a concept vehicle for studying convergence and data rates
    in peer to peer network transmission. The overall purpose is a public view
    project that can assist people in seeing working C++ code in the wild in a
    GNU build environment. In a practical application, DLM can be extracted into
    a utility that can be integrated to other software projects to provide mesh
    -like network behavior when required.

    The general design of DLM is going to be considerably general and attempt to
    utilitize most of the newer features of C++, avoiding TR1 and Boost type
    libraries for the time being (invoking too much magic, and heavy TMP is not
    the design goal of this project).

    Documentation for the interfaces of DLM, where needed, will exist in either 
    a seperate documentation folder or inline with the implementation.

Todo:
    Finish converting and using XTCI.

