#ifndef CACHESIZE_HACK_INCLUDED
#define CACHESIZE_HACK_INCLUDED

#include <cstddef>

#include <windows.h>

size_t cacheline()
{
    using slpi = SYSTEM_LOGICAL_PROCESSOR_INFORMATION;

    std::size_t line_size = 0;
    long unsigned int buffer_size = 0;

    GetLogicalProcessorInformation(nullptr, &buffer_size);

    const std::size_t elements = buffer_size / sizeof(slpi);
    slpi *buffer = new slpi[elements];

    GetLogicalProcessorInformation(buffer, &buffer_size);

    std::size_t i;

    for (i = 0;
         (i < elements) && 
         (
             (buffer[i].Relationship != RelationCache) &&
             (buffer[i].Cache.Level != 1)
         );
         i++)
        ;

    line_size = buffer[i].Cache.LineSize;

    delete[] buffer;

    return line_size;
}

#endif // CACHESIZE_HACK_INCLUDED