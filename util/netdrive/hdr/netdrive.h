#ifndef NETDRIVE_INCLUDED
#define NETDRIVE_INCLUDED

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>

#include <exception>
#include <utility>
#include <cstdio>

#include "../../errata/hdr/errata.h"

namespace net_driver {

class udp_driver;

/***

auto_wsa is a helper class to manage startup and cleanup of the WSA driver.

Construction Parameters:
    - version:
        Specifies the version of WSA required.

***/

class auto_wsa
{
    private:
    WSAData m_wsa_data;

    public:
    explicit auto_wsa(unsigned short int version)
    {
        static const char msg[] = u8"wsa init failure";
        int reason_code = WSAStartup(version, &m_wsa_data);

        if (reason_code != 0)
            throw errata::error(msg, sizeof(msg));
    }

    ~auto_wsa()
    {
        WSACleanup();
    }
};

/***

auto_socket is a helper class to manage the creation and cleanup of individual
sockets.

Construction Parameters
    - See parameter information for socket(); API function, parameters are
      mirrored.

***/

class auto_socket
{
    private:
    int m_endpoint;

    public:
    explicit auto_socket(int family, int type, int protocol)
    : m_endpoint(socket(family, type, protocol))
    {
        static const char msg[] = u8"socket failure";

        if (m_endpoint == SOCKET_ERROR)
            throw errata::error(msg, sizeof(msg));
    }

    ~auto_socket()
    {
        closesocket(m_endpoint);
    }
};

/***

net_driver is the mid-abstraction for WAN/LAN level data management. It will
implement read/write semantics as requested from dlm. This is intended to make
sure that different protocols can all share the same basis class.

Template Arguments:
    - transport_driver:
        Specifies which protocol will be used to read/write network data.
        Defaults to udp_driver with no argument, a basic UDP driven socket
        system.

***/

template <typename transport_driver = class udp_driver>
class net_driver
{
    private:
    // WSA version 2.2
    static constexpr unsigned short int wsa_version = 0x0202;

    // construct network requirements
    auto_wsa wsa_data;
    transport_driver driver;

    public:

    net_driver() try
    : wsa_data(wsa_version)
    {
    }
    catch (errata::error& except)
    {
        static const char msg[] = " <- network init failure";

        except.append_info(msg, sizeof(msg));

        throw except;
    }
};

/***

udp_driver is a basic UDP socket layer meant for use with net_driver.

***/

class udp_driver
{
    private:
    auto_socket endpoint;

    public: 
    udp_driver() try
    : endpoint(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
    {
    }
    catch (errata::error& except)
    {
        static const char msg[] = u8" <- udp init failure";

        except.append_info(msg, sizeof(msg));

        throw except;
    }

};

}
//end namespace net_driver

#endif //NETDRIVE_INCLUDED