#ifndef ERRATA_INCLUDED
#define ERRATA_INCLUDED

#include <cstddef>
#include <exception>

namespace errata {

/***

error is an exception class type meant for specifying (mostly) constructor
initialization failures. Inherits std::exception to provide standard
polymorphic "what()" functionality.

Construction Parameters:
    - msg:
        Contains a pointer to an error message to be copied onto the error
        description stack. (Implemented in terms of append_info(), see Client
        Members).

    - length
        Contains the length in raw 'char's of what msg points to.

Client Members:
    - append_info(error_data, length):
        Allows clients to append further exception information onto the error
        description stack. When this function is used externally (not by the
        constructor), it is expected that a patterned format string be used
        to show the error description chain e. g.:

            [...].append_info(" <- error", length_of_error_string);

        Parameter information can be seen in Construction Parameters.

    - virtual what():
        - This function overrides std::exception's generic .what() request.
          Returns a pointer to the internal error description stack, or
          a detailed std::exception, polymorphically.

***/

class error : public std::exception
{
    private:
    static constexpr std::size_t message_size = 256;
    char m_msg[message_size] = { 0 };
    size_t m_charsize;

    public:
    explicit error(const char *const msg, std::size_t length)
    : m_charsize(0)
    {
        append_info(msg, length);
    }

    virtual const char *what() const noexcept override
    {
        return m_msg;
    }

    void append_info(const char *const error_data, std::size_t length)
    {            
        // clamp message boundary
        std::size_t ceiling = message_size - m_charsize;

        length = (length < ceiling) ? length : ceiling;
        memcpy(&m_msg[m_charsize], error_data, length);
        m_charsize += length - 1;
    }
};

} // end namepace net_driver

#endif // ERRATA_INCLUDED