#ifndef DLM_INCLUDED
#define DLM_INCLUDED

#include <cstddef>
#include <limits>

#include "../../netdrive/hdr/netdrive.h"

namespace dlm {

// check for sane environment (we're not on a DSP)
static_assert(std::numeric_limits<unsigned char>::digits == 8,
              "We're not in Kansas anymore...");

/***

dlm contains the highest tier abstraction code to implement the n-many
network graph. This means dlm has no understanding of networking; the data
source is agnostic to the class, but known to the client.

Template Parameters:
    - data_driver:
        Indicates the class that implements data read and write methods. The
        overall design of identifying nodes is still being thought out.

Construction Parameters:
    - None, Default:
        Based on the specified template parameter, all lower level construction
        is delegated to data_driver.

***/

template <typename data_driver>
class dlm
{
    private:
    data_driver driver;

    public:
    explicit dlm() try
    {
    }
    catch (errata::error& except)
    {
        static const char msg[] = u8" <- dlm init failure";

        except.append_info(msg, sizeof(msg));

        throw except;
    }

    void block_read(std::size_t buffer_size)
    {
    }
};

}
// end namespace dlm

#endif //DLM_INCLUDED