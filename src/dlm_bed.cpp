#include <cstdio>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <conio.h>

#include "../util/dlm/hdr/dlm.h"
#include "../util/netdrive/hdr/netdrive.h"
#include "../../util/xtci/hdr/xtci.h"

// #include "../util/misc/hdr/cachesize_hack.h" for testing purposes

// function try block (not normal to use in this fashion, but okay here
int main() try
{
	// simplify scoping, pull in feature set
	using data_driver = net_driver::net_driver<>;

	dlm::dlm<data_driver> graph;
	xtci::xtci adv_scr;

	adv_scr.active_coord(xtci::mode::dbg_box);
	adv_scr.active();

	const static char16_t test_str[] =
		u"Type \'cls\' to clear screen, or \'quit\' to exit.\r\n";

	const static char16_t testing_str[] =
		u"Extreme amounts of text that make sure that it overruns the buffer in"
		u" the screen";

	const static char16_t then_more[] = u"then some more text\r\n";

	adv_scr.output(test_str, sizeof(test_str) / sizeof(*test_str) - 1);
	adv_scr.output(testing_str, sizeof(testing_str) / sizeof(*testing_str) - 1);
	adv_scr.output(then_more, sizeof(then_more) / sizeof(*then_more) - 1);

	static char16_t quit_str[] = u"quit";
	static char16_t clear_str[] = u"cls";

	char16_t buffer[128];
	size_t char_num = 0;

	auto is_wstr_wmax = [&char_num](
		const char16_t *buffer,
		const auto& cmp_str
	) -> bool
	{
		constexpr size_t char_cmp =
			sizeof(cmp_str) / sizeof(*cmp_str) - 1;

		return wcsncmp(
			reinterpret_cast<const wchar_t *>(buffer),
			reinterpret_cast<const wchar_t *>(cmp_str),
			char_cmp < char_num ? char_cmp : char_num
		) == 0;
	};

	do
	{
		using buff_state = xtci::buff_state;
		buff_state state;

		do
		{
			WaitForSingleObject(adv_scr.get_waitable(), INFINITE);
		} while ((state = adv_scr.accumulate()) == buff_state::pending);

		if (state == buff_state::complete)
		{
			const char16_t *const remote = adv_scr.input(char_num);

			memcpy(buffer, remote, char_num * sizeof(*remote));
		}
		
		buffer[char_num++] = '\r';
		buffer[char_num++] = '\n';
		adv_scr.output(buffer, char_num);

		if (is_wstr_wmax(buffer, clear_str))
			adv_scr.clear_output();
	} while(!is_wstr_wmax(buffer, quit_str));

	return 0; 
}
catch (std::exception& except)
{
    // run-time polymorphically shout what happened/log (possibly)
    fputs(except.what(), stdout);
}
